# Setup SFTP on GCP

This tutorial help you to setup `sftp`  tool which makes the file transfer easier between GCP and your computer.

  * MacOS/Linux users
  * Windows users

[TOC]

## 1. Generate Key-pair
To access the files on google cloud computing engine for your local machine, you need to first setup a new key-pair.
The setup process is different for Mac and Windows user.

#### MacOS

1. Open a terminal and use ssh-keygen to generate the key-pair. USERNAME is your google account id.

        ssh-keygen -t rsa -f ~/.ssh/my-ssh-key -C [USERNAME]

2. You will be prompted to enter a password (length > 4). **Please remember it for future use.**

        Generating public/private rsa key pair.
        Enter passphrase (empty for no passphrase): 
        Enter same passphrase again: 
        Your identification has been saved in /Users/lj/.ssh/gcloud-ssh-key.
        Your public key has been saved in /Users/lj/.ssh/gcloud-ssh-key.pub.
                
3. show the public key in your terminal. Copy the key.

        cat /Users/lj/.ssh/my-ssh-key.pub
        # you should see similar output in the terminal
        ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8eZODHrEZyOZk9FLfRIMGXBGMRXr
        zW3V8+6DlmX02OlDjFMHBmEbAsdfccvvcns6ieX0CNqbollU49uaAQoIkYYVzPKw3TV
        nP0UjO9m7Lsy7AnwHtrYmWXusTiLf8Rd9en7qrHnaJdtQfhhEF+FazlOVQyCjoEK1HV
        0VP2sXl1oVkIBEafd(dfcfdvBw3OKYBjDxb4+3/qJJw0aqqUtcGTH7zAtcemvJJKDgD
        WehTtadfLEPbTM2PxUuPz+ddfc3SM/wtyzz9wjW5uYoiVdd7Caqhn61ut+++batpM3T
        SgYCKwLzKozWjw278vnCKfFjL42yyBYzLBDHJhoaws81l USERNAME


#### Windows
To generate a new SSH key-pair on Windows workstations:

1. Download [puttygen.exe](https://the.earth.li/~sgtatham/putty/latest/w32/puttygen.exe).

2. Run PuTTYgen. A window opens where you can configure your key generation settings.

    ![puttygene open](/images/pg_init.PNG/?raw=true "PuTTYgen")

3. Click the Generate button to generate a new key-pair. You need to move the mouse around to generate some randomness. 

    ![puttygene generate](/images/pg_generate.PNG/?raw=true "PuTTYgen")
    
4. When you are done generating the key-pair, the tool displays your public key value.

    ![puttygene display](/images/pg_pubkey.PNG/?raw=true "PuTTYgen")

5. In the Key comment section, enter your Google username. Enter a Key passphrase to protect your key.
6. Click Save private key to save the private key to a file. For this example, save the key as gcloud-ssh-key.ppk.
7. Click Save public key to write your public key to a file for use later. Keep the PuTTYgen window open for now.
8. Copy all the key in the red block showed in step 4.

## 2. Add SSH key to google cloud

[Goto Metadata SSH Keys](https://console.cloud.google.com/compute/metadata/sshKeys?_ga=1.119245897.1305597412.1487254090&project=isb-cgc-04-0016)

1. Click SSH Keys. It will should a list of keys you currently have.
2. Click Edit.
3. Click + Add item. Paste the key value you generated in Terminal(MacOS) or PuTTYgen(Windows) as a new item.
4. Click Save.

    ![add key google](/images/addsshkey.png/?raw=true "add key")

## 3. Connect to the cloud
You need to get the external IP address here.

![ssh](/images/establish-ssh-connection-1.png?raw=true "ssh access")

#### MacOS
1. Download and install [Cyberduck](https://update.cyberduck.io/Cyberduck-5.3.7.23440.zip)
2. Open Cyberduck
3. Click "+" sign at the lower left corner.

    ![duck](/images/duck.png?raw=true "duck")
    
4. Select SFTP from the top drop-down menu, and enter a name for the connection.
5. Enter external IP in Server; Enter google id in Username; check Use Public Authentication.
6. Locate the private key in .ssh/ folder/
7. Double-click the connection to login.

    
#### Windows
1. Download [WinScp](https://winscp.net/download/WinSCP-5.9.4-Portable.zip)
2. Unzip the file and open the unziped folder.
3. Double-click the WinSCP.exe file.
4. click on the new site

    ![winscp](/images/winscp.png?raw=true "winscp")
    
5. Copy your external IP address into Host anme; Enter you google id into User name; Enter the password you used when generated the key.
6. Click Advanced
7. Click SSH -> Authentication
8. Locate the private key file.
9. Save the profile
10. Login

![winscp](/images/winscp_key.png?raw=true "winscp")



 